package com.example.student.todoproject;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class TodoAdapter extends RecyclerView.Adapter<TodoPostHolder> {

    private ArrayList<TodoPost> todoPost;
    private ActivityCallback activityCallback;
    private TodoPostHolder holder;

    public TodoAdapter(ActivityCallback activityCallback)  {
        this.activityCallback = activityCallback;
    }

    public ArrayList<TodoPost> getTodoPosts() {
        return todoPost;
    }

    @Override
    public TodoPostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new TodoPostHolder(view);
    }

    @Override
    public void onBindViewHolder(TodoPostHolder toDoAdapter, int i) {
        holder.titleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v){

                activityCallback.onPostSelected(todoUri);
            }
        });
    }


    @Override
    public int getItemCount() {
        return todoPosts.size();
    }
}
