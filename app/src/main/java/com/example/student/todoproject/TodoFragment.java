package com.example.student.todoproject;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class TodoFragment extends Fragment {

    private RecyclerView recyclerView;
    private ActivityCallback activityCallback;
    private ToDoActivity activity;

    @Override
    public void onAttach( Activity activity) {
        activityCallback = (ActivityCallback)activity;
        this.activity = (ToDoActivity)activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_fragment, container, false);
        return view;
    }
}

