package com.example.student.todoproject;

import android.net.Uri;

public interface ActivityCallback {
    void onPostSelected(Uri todoPostUri);
}
