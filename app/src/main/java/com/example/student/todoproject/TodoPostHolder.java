package com.example.student.todoproject;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class TodoPostHolder extends RecyclerView.ViewHolder{
    public TextView titleText;

    public TodoPostHolder(View itemView) {
        super(itemView);
        titleText = (TextView)itemView;
    }
}
