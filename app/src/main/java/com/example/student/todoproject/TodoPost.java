package com.example.student.todoproject;

public class TodoPost {

    public String title;
    public String dateAdded;
    public String dateDue;
    public String category;

    public TodoPost(String title, String dateAdded, String dateDue, String category) {
        this.title = title;
        this.dateAdded = dateAdded;
        this.dateDue = dateDue;
        this.category = category;
    }
}

